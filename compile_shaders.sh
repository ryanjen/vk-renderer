#!/bin/sh

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

SHADER_SRC_DIR="$SCRIPT_DIR/src/shaders/"
SHADER_BUILD_DIR="$SCRIPT_DIR/build/spirv"


for F in $(ls $SHADER_SRC_DIR | egrep "*.frag|*.tesc|*.tese|*.geom|*.vert|*.comp"); do
    glslangValidator -V "$SHADER_SRC_DIR/$F" -o "$SHADER_BUILD_DIR/$F.spv"
done
