#ifndef _APPLICATION_
#define _APPLICATION_

#include <GLFW/glfw3.h>
#include <memory>
#include <vulkan/vulkan.h>

#include "SwapchainManager.hpp"
#include "VulkanDestroyer.h"
#include "Utility.hpp"
#include <vector>
#include <thread>
#include <optional>
#include <cstring>

#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vulkan/vulkan_core.h>


namespace VkRenderer {

// forward decl
class ResourceCache;


struct DescriptorSetTemplate {
    VkDestroyer(VkDescriptorPool) pool;
    VkDestroyer(VkDescriptorSetLayout) layout;
};

class Application {
    public:
        VkDestroyer(LibraryType) library;
        VkDestroyer(VkInstance) instance;
        VkPhysicalDevice physicalDevice;
        VkDestroyer(VkDevice) device;
        VkDestroyer(VmaAllocator) allocator;

        VkQueue graphicsQueue;
        VkQueue computeQueue;
        VkQueue presentQueue;
        //VkQueue transferQueue;
        uint32_t graphicsFamily;
        uint32_t computeFamily;
        uint32_t presentFamily;
        //uint32_t transferFamily;

        VkDestroyer(VkSurfaceKHR) surface;
        SwapchainManager swapchainManager;

        GLFWwindow *window;

        std::unique_ptr<ResourceCache> cache;

        DescriptorSetTemplate mvpDS;
        DescriptorSetTemplate texLookupDS;

        VkPushConstantRange texidPcRange;

        VkDestroyer(VkRenderPass) renderpass;

        VkDestroyer(VkPipelineLayout) graphicsPipelineLayout;
        VkDestroyer(VkPipeline) graphicsPipeline;
        
        Application();
        void init();
        ~Application();

        void renderloop();

        bool loadScene(const std::vector<std::string> & modelPaths, std::vector<glm::vec3> positions);

    private:
        std::vector<FrameResources> frameResources;
        Scene scene;

        void initGLFW();
        void createWindow();
        void createSurface();
        void registerGLfwCallbacks();
        void loadGlobalFunctions();
        void createInstance();
        void loadInstanceFunctions();        
        void choosePhysicalDeviceAndQueueFamilies();
        void createLogicalDevice();
        void loadDeviceFunctions();
        void initVmaAllocator();
        void initGlobalContext();
        void initResourceCache();
        void getQueues();
        void initDSTemplates();
        void createRenderpass();
        void createGraphicsPipelineLayout();
        void createGraphicsPipeline();

        void createFramebuffer(uint32_t swapchainIdx, VkImageView depthView, VkFramebuffer & resultFramebuffer);
        void allocatePerFrameDescriptorSets();
        void allocateMvpUniformBuffers();
        void updateMvpDescriptorSets();
        void createDepthBuffers();
        void initFrameResources();

        void updatePerModelData(FrameResources & frame_resources, ModelInstance & model);
        void updatePerFrameData(FrameResources & frame_resources);

        void updateTexLookupArrayDescriptorSets();

        // intended to be used with concurrent frame recording
        std::thread dispatchFrameRecording(FrameResources & resources);

        void recordSingleFrame(FrameResources & resources);
        void submitSingleFrame(FrameResources & resources);

        void renderLoopSequentialFrames();
        void renderLoopConcurrentFrames();

        std::optional<Model> loadModel(const std::string& path, bool triangulate);

        void createCommandPoolBufferPair(VkCommandPool & resultPool, VkCommandBuffer & resultBuffer) {
            VkCommandPoolCreateInfo poolInfo {
                .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
                .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
                .queueFamilyIndex = this->graphicsFamily
            };

            VK_ASSERT( vkCreateCommandPool(*this->device, &poolInfo, nullptr, &resultPool) );

            VkCommandBufferAllocateInfo allocInfo {
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                .commandPool = resultPool,
                .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                .commandBufferCount = 1,
            };
            VK_ASSERT( vkAllocateCommandBuffers(*this->device, &allocInfo, &resultBuffer ) );
        }
};

}

#endif