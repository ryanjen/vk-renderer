#ifndef _UTILITY_
#define _UTILITY_

#include "Common.h"

#include "VulkanDestroyer.h"
#include "config.hpp"

#include <string>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <memory>
#include <iostream>
#include <optional>
#include <sstream>
#include <memory>
#include <vector>
#include <vulkan/vulkan_core.h>


namespace VkRenderer {

// I'll distinguish between *Resources (members used for bookkeeping) and other stuff like textureView here (need to simplify all of this)
struct TextureView {
    VkImageView imageView;
    VkSampler sampler;
};

struct Material {
    TextureView diffuseTex;
};

struct UniformBuffer {
    VkBuffer buffer;
    VmaAllocation allocation;
    void *mappedMemory;
    VkMemoryPropertyFlags memType;

    UniformBuffer(): buffer(VK_NULL_HANDLE), allocation(VK_NULL_HANDLE), mappedMemory(nullptr) {}

    UniformBuffer & operator=(UniformBuffer&& rhs) {
        this->buffer = rhs.buffer;
        this->allocation = rhs.allocation;
        this->mappedMemory = rhs.mappedMemory;
        this->memType = rhs.memType;

        rhs.buffer = VK_NULL_HANDLE;
        rhs.allocation = VK_NULL_HANDLE;
        rhs.mappedMemory = nullptr;
        return *this;
    }

    UniformBuffer(UniformBuffer&& rhs) {
        *this = std::move(rhs);
    }

    ~UniformBuffer() {
        if (mappedMemory)
            vmaUnmapMemory(vmaContext.allocator, allocation);
        vmaDestroyBuffer(vmaContext.allocator, buffer, allocation);
    }
};

struct StagingBuffer {
    VkBuffer buffer;
    VmaAllocation allocation;
    void *mappedMemory;

    StagingBuffer(): buffer(VK_NULL_HANDLE), allocation(VK_NULL_HANDLE), mappedMemory(nullptr) {}

    StagingBuffer & operator=(StagingBuffer&& rhs) {
        this->buffer = rhs.buffer;
        this->allocation = rhs.allocation;
        this->mappedMemory = rhs.mappedMemory;

        rhs.buffer = VK_NULL_HANDLE;
        rhs.allocation = VK_NULL_HANDLE;
        rhs.mappedMemory = nullptr;
        return *this;
    }

    StagingBuffer(StagingBuffer&& rhs) {
        *this = std::move(rhs);
    }

    ~StagingBuffer() {
        if (mappedMemory)
            vmaUnmapMemory(vmaContext.allocator, allocation);
        vmaDestroyBuffer(vmaContext.allocator, buffer, allocation);
    }
};

struct VertexBuffer {
    VkBuffer buffer;
    VmaAllocation allocation;

    VertexBuffer & operator=(VertexBuffer&& rhs) {
        this->buffer = rhs.buffer;
        this->allocation = VK_NULL_HANDLE;

        rhs.buffer = VK_NULL_HANDLE;
        rhs.allocation = VK_NULL_HANDLE;
        return *this;
    };

    VertexBuffer(VertexBuffer&& rhs) {
        *this = std::move(rhs);
    }

    VertexBuffer(): buffer(VK_NULL_HANDLE), allocation(VK_NULL_HANDLE) {}

    ~VertexBuffer() {
        vmaDestroyBuffer(vmaContext.allocator, buffer, allocation);
    }
};

struct TextureResources {
    VkDestroyer(VkImageView) imageView;
    VkDestroyer(VkSampler) sampler;
    VkImage image;
    VmaAllocation allocation;
    VkExtent3D extent;
    VkFormat format;

    TextureResources &operator=(TextureResources&& rhs) {
        this->image = rhs.image;
        this->allocation = rhs.allocation;
        this->extent = rhs.extent;
        this->format = rhs.format;
        this->imageView = std::move(rhs.imageView);
        this->sampler = std::move(rhs.sampler);
        std::cout << "move: " << *this->imageView << std::endl;

        rhs.image = VK_NULL_HANDLE;
        rhs.allocation = VK_NULL_HANDLE;
        rhs.extent = {0, 0, 0};
        return *this;
    }

    TextureResources(TextureResources&& rhs) {
        *this = std::move(rhs);
    }

    TextureResources(): image(VK_NULL_HANDLE), allocation(VK_NULL_HANDLE) {}

    ~TextureResources() {
        vmaDestroyImage(vmaContext.allocator, image, allocation);
    }
};

struct DepthBufferResources {
    VkImage depthBuffer;
    VkImageView depthView;
    VmaAllocation allocation;

    DepthBufferResources &operator=(DepthBufferResources&& rhs) {
        this->depthBuffer = rhs.depthBuffer;
        this->depthView = rhs.depthView;
        this->allocation = rhs.allocation;

        rhs.depthBuffer = VK_NULL_HANDLE;
        rhs.depthView = VK_NULL_HANDLE;
        rhs.allocation = VK_NULL_HANDLE;
        return *this;
    }

    DepthBufferResources(DepthBufferResources&& rhs) {
        *this = std::move(rhs);
    }

    DepthBufferResources(): depthBuffer(VK_NULL_HANDLE), depthView(VK_NULL_HANDLE), allocation(VK_NULL_HANDLE) {}

    ~DepthBufferResources() {
        vkDestroyImageView(vmaContext.device, depthView, nullptr);
        vmaDestroyImage(vmaContext.allocator, depthBuffer, allocation);
    }
};

// the descriptor sets and buffers are pretty much 1-1 for now
struct RasterizationPipelineDSets {
    VkDescriptorSet mvpSet;
    VkDescriptorSet texLookupDSet;
};


//typedef FragShaderTexLookupArray std::array<>

struct RasterizationPipelineUniformBuffers {
    UniformBuffer mvpBuffer;
};

struct FrameResources {
    VkDestroyer(VkSemaphore) imageAcquired;
    VkDestroyer(VkSemaphore) presentable;
    VkDestroyer(VkFence) imageReleased;
    VkDestroyer(VkFramebuffer) framebuffer;
    DepthBufferResources depthBuffer;
    // acquireSwapchainImage | render | present
    VkDestroyer(VkCommandPool) commandPool;
    RasterizationPipelineDSets descriptorSets;
    RasterizationPipelineUniformBuffers uniformBuffers;
    VkCommandBuffer commandBuffer;
    uint32_t imageIndex;
    bool command_buffer_out_of_date;
};

// hardcoding this for now until I can render wireframe
struct Vertex {
    float x;
    float y;
    float z;
    //float nx;
    //float ny;
    //float nz;
    float tx;
    float ty;

    std::string toString();
};


struct Face {
    uint32_t firstIdx;
    uint32_t numVertices;

    // for later TODO
    int32_t matIdx;     // could be 8_t
    //uint8_t texIdx;
};


struct Mesh {
    std::vector<Vertex> data;
    std::vector<Face> faces;

    std::string toString();
};


struct ModelInstance {
    uint32_t modelIdx;
    glm::vec3 scenePosition;
};


struct Model {
    Mesh mesh;

    VkBuffer vertexBuffer;

    // Current approaches to selecting texture inside shader:
    //  -Load every texture of model with texture sampler array and use push constants in between draw calls to change idx
    //  -multi-draw indirect
    //  -use smarter system to dynamically map Vk handle -> texture idx, so we can keep common textures available across models,
    //      maybe use LRU eviction upon trying to load entire model
    //  -split mesh into parts based on texture; bind tex once before drawing each face in part 
    // I'll probably use #1 first because it's simple

    std::vector<Material> materials;

    std::string toString();
};


struct Scene {
    std::vector<Model> models;
    std::vector<ModelInstance> instances;
};

VkShaderModule loadShaderModule(VkDevice device, const std::string fname);

std::optional<Model> loadModel(const std::string& path, bool triangulate);

std::optional<VkImage> createImage();


} // namespace VkRenderer

#endif // _UTILITY_