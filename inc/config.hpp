#ifndef _CONFIG_
#define _CONFIG_

#include <cstdint>
#include <vulkan/vulkan.h>
#include <math.h>
#include <vector>
#include <string>
#include <cassert>
#include <vulkan/vulkan_core.h>

// common macros
#define VK_ASSERT( expr ) assert( (expr) == VK_SUCCESS )

namespace VkRenderer {

#if defined _WIN32
typedef HMODULE LibraryType;
#elif defined __linux
typedef void * LibraryType;
#endif

#define API_VERSION VK_MAKE_VERSION(1, 2, 0)

// constants/config
static const VkExtent2D initial_window_size = { 640, 480 };

// number of frames to draw concurrently. Swapchain might restrict to fewer
static const uint32_t max_concurrent_frames = 3;

static const bool record_frames_concurrently = false;

static const uint64_t fence_timeout = 2000000000;

static const std::vector<const char *> desiredInstanceLayers = {
    "VK_LAYER_KHRONOS_validation",
};

static std::vector<const char *> desiredInstanceExtensions = {
    VK_KHR_SURFACE_EXTENSION_NAME,
    "VK_KHR_get_physical_device_properties2"
};

const std::vector<const char *> desiredDeviceExtensions = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    "VK_EXT_descriptor_indexing",
    //"GL_EXT_nonuniform_qualifier",
    //"SPV_EXT_descriptor_indexing",
    "VK_KHR_maintenance3",
    "VK_KHR_bind_memory2",
    "VK_KHR_dedicated_allocation",
    "VK_KHR_get_memory_requirements2",
    //"VK_KHR_get_physical_device_properties2"
    

};

static const std::vector<std::string> mainSceneModels = {
        "../Models/triangle2.obj"
};


} // namespace VkRenderer

#endif // _CONFIG_