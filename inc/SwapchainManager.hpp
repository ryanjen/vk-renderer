#ifndef _SWAPCHAIN_MANAGER
#define _SWAPCHAIN_MANAGER

#include "Common.h"
#include "Utility.hpp"
#include "config.hpp"

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#include <vulkan/vulkan_core.h>
#include <iostream>
#include <algorithm>
#include <optional>

namespace VkRenderer {

class SwapchainManager {
    private:
        VkDevice device;
        VkPhysicalDevice physicalDevice;
        GLFWwindow * window;

    public:
        VkSurfaceKHR surface;
        VkSwapchainKHR swapchain;

        struct SwapchainImage {
            VkImage image;
            VkImageView view;
        };

        std::vector<SwapchainImage> images;
        VkSurfaceFormatKHR format;
        VkSurfaceCapabilitiesKHR surfaceCapabilities;
        VkExtent2D extent;
        bool windowResized;

        SwapchainManager(): device(VK_NULL_HANDLE), physicalDevice(VK_NULL_HANDLE), surface(VK_NULL_HANDLE),
                    swapchain(VK_NULL_HANDLE), windowResized(false) {}

        void init(VkDevice device, VkPhysicalDevice physicalDevice, GLFWwindow *window, VkSurfaceKHR surface) {
            this->device = device;
            this->physicalDevice = physicalDevice;
            this->window = window;
            this->surface = surface;
        }

        bool createSwapchain() {
            assert( surface != VK_NULL_HANDLE );
            // same for now
            return recreateSwapchain();
        }


        bool recreateSwapchain() {
            std::cout << "Recreating swapchain" << std::endl;
            uint32_t numFormats;
            std::vector<VkSurfaceFormatKHR> formats;
            VK_ASSERT( vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, this->surface, &numFormats, nullptr) );
            formats.resize(numFormats);
            VK_ASSERT( vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &numFormats, formats.data()) );

            VkSurfaceFormatKHR desiredFormat = { .format = VK_FORMAT_B8G8R8_UNORM, .colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
            bool formatSupported = false;
            assert(formats.size() > 0);
            for (auto & available: formats) {
                if (available.format == desiredFormat.format && available.colorSpace == desiredFormat.colorSpace) {
                    formatSupported = true;
                    break;
                }
            } 
            VkSurfaceFormatKHR actualFormat = formatSupported ? desiredFormat : formats[0]; 

            VK_ASSERT( vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &this->surfaceCapabilities) ); 

            // update current extent
            if (this->surfaceCapabilities.currentExtent.width != ~0)
                this->extent = surfaceCapabilities.currentExtent;
            else {
                int width;
                int height;
                glfwGetFramebufferSize(this->window, &width, &height);
                this->extent = {
                    std::clamp((uint) width, surfaceCapabilities.minImageExtent.width, surfaceCapabilities.maxImageExtent.width),
                    std::clamp((uint) height, surfaceCapabilities.minImageExtent.height, surfaceCapabilities.maxImageExtent.height)
                };
            }

            VkSwapchainCreateInfoKHR swapchainInfo = {
                .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
                .surface = this->surface,
                .minImageCount = this->surfaceCapabilities.minImageCount,
                .imageFormat = actualFormat.format,
                .imageColorSpace = actualFormat.colorSpace,
                .imageExtent = surfaceCapabilities.currentExtent,
                .imageArrayLayers = 1,
                .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
                .preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,
                .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
                .presentMode = VK_PRESENT_MODE_FIFO_KHR,
                .clipped = VK_TRUE,
                .oldSwapchain = this->swapchain
            };

            VkResult res = vkCreateSwapchainKHR(device, &swapchainInfo, nullptr, &this->swapchain);
            if (res != VK_SUCCESS)
                return false;
            this->format = actualFormat;

            std::vector<VkImage> swapchainImages;

            this->images.clear();
            uint32_t numImages;
            res = vkGetSwapchainImagesKHR(this->device, this->swapchain, &numImages, nullptr);
            if (res != VK_SUCCESS)
                return false;
            swapchainImages.resize(numImages);
            res = vkGetSwapchainImagesKHR(this->device, this->swapchain, &numImages, swapchainImages.data());
            if (res != VK_SUCCESS)
                return false;            

            this->images.resize(numImages);

            for (uint32_t i = 0; i < swapchainImages.size(); i++) {
                VkImageSubresourceRange range {
                    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                    .baseMipLevel = 0,
                    .levelCount = VK_REMAINING_MIP_LEVELS,
                    .baseArrayLayer = 0,
                    .layerCount = VK_REMAINING_ARRAY_LAYERS,
                };

                VkImageViewCreateInfo viewInfo {
                    .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
                    .image = swapchainImages[i],
                    .viewType = VK_IMAGE_VIEW_TYPE_2D,
                    .format = actualFormat.format,
                    .components = {
                        VK_COMPONENT_SWIZZLE_IDENTITY,
                        VK_COMPONENT_SWIZZLE_IDENTITY,
                        VK_COMPONENT_SWIZZLE_IDENTITY,
                        VK_COMPONENT_SWIZZLE_IDENTITY
                    },
                    .subresourceRange = range
                };

                this->images[i].image = swapchainImages[i];
                vkCreateImageView(this->device, &viewInfo, nullptr, &this->images[i].view);
            }

            return true;
        }

        
        bool acquireImage(VkSemaphore signalSemaphore, uint32_t & resultIdx) {
            VkResult res;
            while ( VK_ERROR_OUT_OF_DATE_KHR == 
                            (res = vkAcquireNextImageKHR(this->device, this->swapchain, fence_timeout, signalSemaphore, VK_NULL_HANDLE, &resultIdx)) ) {
                recreateSwapchain();
            }
            switch (res) {
                case VK_SUCCESS:
                case VK_SUBOPTIMAL_KHR:
                    return true;
                default:
                    return false;
            }
        }

        void presentQueueSubmit(FrameResources & resources, VkQueue presentQueue) {
            VkPresentInfoKHR present_info {
                .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
                .waitSemaphoreCount = 1,
                .pWaitSemaphores = &(*resources.presentable),
                .swapchainCount = 1,
                .pSwapchains = &this->swapchain,
                .pImageIndices = &resources.imageIndex,
                .pResults = nullptr
            };

            VkResult res = vkQueuePresentKHR(presentQueue, &present_info);

            if (this->windowResized) {
                this->windowResized = false;
                recreateSwapchain();
                return;
            }

            switch (res) {
                case VK_SUCCESS:
                case VK_SUBOPTIMAL_KHR:
                    break;
                case VK_ERROR_OUT_OF_DATE_KHR:
                    assert(recreateSwapchain());
                    break;
                default:
                    std::cout << "VkQueueSubmit error: " << res << std::endl;
                    exit(1);
            }
        }
};


// 

} // namespace VkRenderer
#endif


// create swapchain

// destroy swapchain

