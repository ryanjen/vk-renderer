#ifndef _COMMON_
#define _COMMON_

#define VK_NO_PROTOTYPES 1
#include "VulkanFunctions.h"

#include "vk_mem_alloc.h"


struct VmaContext {
    VkDevice device;
    VmaAllocator allocator;
};

// this is pretty ugly for now
#ifdef INIT_GLOBALS
    VmaContext vmaContext;
#else
    extern VmaContext vmaContext; 
#endif

#endif