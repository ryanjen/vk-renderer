#ifndef _RESOURCE_CACHE_
#define _RESOURCE_CACHE_

#include "Common.h"
#include "Utility.hpp"
#include "VulkanDestroyer.h"
#include "stb_image.h"

#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <optional>
#include <deque>
#include <vulkan/vulkan_core.h>


namespace VkRenderer {

// forward declaration for recursive ref
class Application;

class CacheEntry {
    friend class ResourceCache;
    public:
        virtual ~CacheEntry() {};

    private:
        virtual void emitStagingCmd(VkCommandBuffer commandBuffer) {};
};

class VertexBufferEntry: public CacheEntry {
    friend class ResourceCache;

    StagingBuffer stagingBuffer;
    VertexBuffer vertexBuffer;
    VkDeviceSize dataSize;

    VertexBufferEntry(): CacheEntry() {}

    void emitStagingCmd(VkCommandBuffer commandBuffer);
};

class TextureEntry: public CacheEntry {
    friend class ResourceCache;

    StagingBuffer stagingBuffer;
    TextureResources texture;

    TextureEntry(): CacheEntry() {}

    void emitStagingCmd(VkCommandBuffer commandBuffer);
};

// Very dumb for now
// each entry will have it's own staging buffer allocated just in time
class ResourceCache {
    private:
        Application *application;
        std::unordered_map<std::string, std::unique_ptr<CacheEntry>> entries;
        //std::deque<CacheEntry> nodes;        // TODO LRU or something
        uint capacity;


        bool stagingDirty;
        VkDestroyer(VkCommandPool) stagingCommandPool;
        VkCommandBuffer stagingCommandBuffer;


        StagingBuffer createStagingBuffer(VkDeviceSize size);
        void appendTransfer(CacheEntry & entry);

    public:
        ResourceCache(Application *application, uint capacity, 
                    VkCommandPool commandPool,VkCommandBuffer commandBuffer)
                            : application(application), capacity(capacity),
                              stagingCommandBuffer(commandBuffer) {
            *this->stagingCommandPool = commandPool;
        }

        std::optional<VkBuffer> lookupVertexBuffer(const std::string objPath);

        std::optional<VkBuffer> putVertexBuffer(const std::string objPath, const std::vector<Vertex> vertices);

        // temporary solution todo
        struct TextureResult {
            VkImageView imageView;
            VkSampler sampler;
        };

        TextureResult getTexture(const std::string & path);

        // move everything that needs uploading from staging to device local mem
        void uploadPending();


};


} // namespace VkRenderer

#endif