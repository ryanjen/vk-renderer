# version 450

layout ( location = 0 ) in vec3 position;
layout ( location = 1 ) in vec2 texcoord_in;

layout ( set = 0, binding = 0 ) uniform Mvp {
    mat4 transformation;
};

layout ( location = 0 ) out vec2 texcoord_out;

void main() {
    gl_Position = transformation * vec4(position, 1.0);
    texcoord_out = texcoord_in;
}
