# version 450
# extension GL_EXT_nonuniform_qualifier : enable

layout ( location = 0 ) in vec2 texcoord;

layout( set = 1, binding = 0 ) uniform sampler2D[] samplers;

layout( location = 0 ) out vec4 frag_color;

layout( push_constant ) uniform constants {
   int texId;   
} PC;


void main() {
    if (PC.texId >= 0) {
        frag_color = texture(samplers[0], texcoord);
    } else {
      frag_color = vec4(0.0, 1.0, 0.0, 1.0);  
    }
}
