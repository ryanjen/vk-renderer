#include "Common.h"

#include "Utility.hpp"
#include <vulkan/vulkan_core.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <iostream>
#include <string>
#include <optional>
#include <filesystem>

namespace VkRenderer {

std::string Vertex::toString() {
    std::stringstream s;
    s << "[" << x << "," << y << "," << z << "]";
    return s.str();
}


std::string Mesh::toString() {
    std::stringstream s;
    for (uint i = 0; i < faces.size(); i++) {
        Face const& f = faces[i];
        s << "face " << i << ": ";
        for (uint j = 0; j < f.numVertices; j++) {
            s << data[j + f.firstIdx].toString();
            if (j < f.numVertices - 1)
                s << "  ";
        }
        s << std::endl;
    }
    return s.str();
};


std::string Model::toString() {
    return mesh.toString();
}

VkShaderModule loadShaderModule(VkDevice device, const std::string fname) {
    std::vector<unsigned char> data;
    FILE *f = fopen(fname.c_str(), "r");
    assert(f != nullptr);
    
    fseek(f, 0, SEEK_END);
    size_t len = ftell(f);
    fseek(f, 0, SEEK_SET);

    data.resize(len);
    assert( 1 == fread(data.data(), len, 1, f) );
    
    fclose(f);

    VkShaderModuleCreateInfo moduleInfo {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .codeSize = (uint32_t) data.size(),
        .pCode = (uint32_t *) data.data()
    };

    VkShaderModule module;
    VK_ASSERT( vkCreateShaderModule(device, &moduleInfo, nullptr, &module) );
    return module;
}


std::optional<VkImage> createImage() {

}


} // namespace VkRenderer