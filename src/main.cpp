#include "Common.h"

#include "Application.hpp"
#include "SwapchainManager.hpp"

#include "config.hpp"
#include <glm/ext/vector_float3.hpp>
#include <iostream>
#include <chrono>

#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Utility.hpp"

namespace VkRenderer {

} // namespace VkRenderer

using namespace VkRenderer;

int main(int argc, char *argv[]) {
    Application app;
    app.init();

    assert(app.loadScene(mainSceneModels, { {0.0f, 0.0f, 0.0f} }));

    app.renderloop();

    return 0;
}