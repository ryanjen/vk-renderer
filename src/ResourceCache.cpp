#include "ResourceCache.hpp"
#include "Application.hpp"

#include "Common.h"
#include "Utility.hpp"
#include "stb_image.h"

#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <optional>
#include <deque>
#include <vulkan/vulkan_core.h>
#include <cstring>

namespace VkRenderer {



void VertexBufferEntry::emitStagingCmd(VkCommandBuffer commandBuffer) {
    VkBufferCopy range {
        .srcOffset = 0,
        .dstOffset = 0,
        .size = this->dataSize
    };
    vkCmdCopyBuffer(commandBuffer, stagingBuffer.buffer, vertexBuffer.buffer, 1, &range);

    VkBufferMemoryBarrier bufferBarrier {
        .sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
        .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .buffer = vertexBuffer.buffer,
        .offset = 0,
        .size = VK_WHOLE_SIZE  // TODO double check that this works alongside VMA
    };

    vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, 0, 
                    0, nullptr, 1, &bufferBarrier, 0, nullptr);

    std::cout << "emit vbo staging cmd" << std::endl;
}


void TextureEntry::emitStagingCmd(VkCommandBuffer commandBuffer) {
    VkBufferImageCopy range {
        .bufferOffset = 0,
        .bufferRowLength = 0,  // assume row major
        .bufferImageHeight = 0,
        .imageSubresource = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 0,
            .layerCount = 1
        },
        .imageOffset = { 0, 0, 0 },
        .imageExtent = texture.extent
    };

    VkImageMemoryBarrier init_to_transfer_transition {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .srcAccessMask = 0,
        .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = texture.image,
        .subresourceRange = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = VK_REMAINING_MIP_LEVELS,
            .baseArrayLayer = 0,
            .layerCount = VK_REMAINING_ARRAY_LAYERS
        }
    };

    vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
                    0, nullptr, 0, nullptr, 1, &init_to_transfer_transition);

    vkCmdCopyBufferToImage(commandBuffer, stagingBuffer.buffer, texture.image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                    1, &range);

    VkImageMemoryBarrier transfer_to_shader_transition {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = texture.image,
        .subresourceRange = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = VK_REMAINING_MIP_LEVELS,
            .baseArrayLayer = 0,
            .layerCount = VK_REMAINING_ARRAY_LAYERS
        }
    };
    // make sure frag shader is ok for dst stage
    // should be batching these for multiple images
    vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
                    0, nullptr, 0, nullptr, 1, &transfer_to_shader_transition);

    std::cout << "emit tex staging cmd" << std::endl;
}



StagingBuffer ResourceCache::createStagingBuffer(VkDeviceSize size) {
    StagingBuffer result;

    VkBufferCreateInfo bufferInfo {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = size,
        .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices = &application->graphicsFamily
    };

    VmaAllocationCreateInfo allocInfo {
        .usage = VMA_MEMORY_USAGE_CPU_ONLY,
        .requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
        .preferredFlags = 0,
    };

    VK_ASSERT( vmaCreateBuffer(*application->allocator, &bufferInfo, &allocInfo, &result.buffer, &result.allocation, nullptr ) );
    VK_ASSERT( vmaMapMemory(*application->allocator, result.allocation, &result.mappedMemory) );
    return result;
}


void ResourceCache::appendTransfer(CacheEntry & entry) {
    // begin recording
    if ( !stagingDirty ) {
        VkCommandBufferBeginInfo beginInfo {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        };
        // later check that cmdbuf isn't pending and block
        VK_ASSERT( vkBeginCommandBuffer(this->stagingCommandBuffer, &beginInfo) );

        stagingDirty = true;
    }

    entry.emitStagingCmd(this->stagingCommandBuffer);
}


std::optional<VkBuffer> ResourceCache::putVertexBuffer(const std::string objPath, const std::vector<Vertex> vertices) {
    auto it = entries.find(objPath);
    if (it != entries.end()) {
        std::cout << "Warning putVertexBuffer(): vertex buffer with path "<< objPath << " already exists" << std::endl;
    }

    VkDeviceSize dataSize = vertices.size() * sizeof(Vertex);

    VkBuffer buffer;
    VmaAllocation allocation;

    VkBufferCreateInfo bufferInfo {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = dataSize,
        .usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices = &application->graphicsFamily
    };

    VmaAllocationCreateInfo allocInfo {
        .usage = VMA_MEMORY_USAGE_GPU_ONLY,
        .requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        .preferredFlags = 0,
    };  

    VK_ASSERT( vmaCreateBuffer(*application->allocator, &bufferInfo, &allocInfo, &buffer, &allocation, nullptr) );

    // create staging buffer
    // map staging memory
    // copy vertex data into memory
    // emit staging command
    std::unique_ptr<VertexBufferEntry> newEntry(new VertexBufferEntry);
    newEntry->vertexBuffer.buffer = buffer;
    newEntry->vertexBuffer.allocation = allocation;
    newEntry->stagingBuffer = createStagingBuffer(dataSize);
    newEntry->dataSize = dataSize;
    memcpy(newEntry->stagingBuffer.mappedMemory, vertices.data(), dataSize);
    VK_ASSERT( vmaInvalidateAllocation(*application->allocator, newEntry->stagingBuffer.allocation, 0, VK_WHOLE_SIZE) );

    std::cout << "invalidating vbo staging mem" << std::endl;

    appendTransfer(*newEntry);

    entries[objPath] = std::move(newEntry);
    return buffer;
}


ResourceCache::TextureResult ResourceCache::getTexture(const std::string & path) {
    auto it = entries.find(path);
    if (it != entries.end()) {
        TextureEntry *entry = dynamic_cast<TextureEntry *>(it->second.get());
        return {
            .imageView = *entry->texture.imageView,
            .sampler   = *entry->texture.sampler
        };
    }

    int width, height, channels;
    unsigned char* imageData = stbi_load(path.c_str(), &width, &height, &channels, STBI_rgb_alpha);
    VkDeviceSize imageSize = width * height * channels;

    VmaAllocationCreateInfo vmaInfo {
        .usage = VMA_MEMORY_USAGE_GPU_ONLY,
        .requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        .preferredFlags = 0
    };

    VkFormat imageFormat;
    if (channels == 3) {
        imageFormat = VK_FORMAT_R8G8B8_SRGB;
    } else if (channels == 4) {
        imageFormat = VK_FORMAT_R8G8B8A8_SRGB;
    } else {
        std::cout << "image formats" << std::endl;
        exit(1);
    }

    VkImage image;
    VmaAllocation allocation;
    VkImageView imageView;
    VkSampler sampler = VK_NULL_HANDLE;

    VkImageCreateInfo imageInfo {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = imageFormat,
        .extent = {
            .width = (uint32_t) width,
            .height = (uint32_t) height,
            .depth = 1,
        },
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices = &application->graphicsFamily,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
    };

    VK_ASSERT( vmaCreateImage(*application->allocator, &imageInfo, &vmaInfo, &image, &allocation, nullptr) );

    // TODO clean this up

    VkSamplerCreateInfo samplerInfo {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .magFilter = VK_FILTER_NEAREST,
        .minFilter = VK_FILTER_NEAREST,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .mipLodBias = 0.0f,
        .anisotropyEnable = false,
        .maxAnisotropy = 1.0f,
        .compareEnable = false,
        .minLod = 0.0f,
        .maxLod = 1.0f,
        .borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK,
        .unnormalizedCoordinates = false
    };

    VK_ASSERT( vkCreateSampler(*application->device, &samplerInfo, nullptr, &sampler) );

    VkImageViewCreateInfo view_info {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = image,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = imageFormat,
        .components = {
            .r = VK_COMPONENT_SWIZZLE_IDENTITY,
            .g = VK_COMPONENT_SWIZZLE_IDENTITY,
            .b = VK_COMPONENT_SWIZZLE_IDENTITY,
            .a = VK_COMPONENT_SWIZZLE_IDENTITY
        },
        .subresourceRange = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = VK_REMAINING_MIP_LEVELS,
            .baseArrayLayer = 0,
            .layerCount = VK_REMAINING_ARRAY_LAYERS
        }
    };

    VK_ASSERT( vkCreateImageView(*application->device, &view_info, nullptr, &imageView));

    std::unique_ptr<TextureEntry> newEntry(new TextureEntry);
    *newEntry->texture.imageView = imageView;
    *newEntry->texture.sampler = sampler;
    newEntry->texture.allocation = allocation;
    newEntry->texture.image = image;
    newEntry->texture.extent = { (uint32_t) width, (uint32_t) height, 1 };
    newEntry->texture.format = imageFormat;
    newEntry->stagingBuffer = createStagingBuffer(imageSize);
    memcpy(newEntry->stagingBuffer.mappedMemory, imageData, imageSize);
    VK_ASSERT( vmaInvalidateAllocation(*application->allocator, newEntry->stagingBuffer.allocation, 0, VK_WHOLE_SIZE) );

    std::cout << "invalidating tex staging mem" << std::endl;
    appendTransfer(*newEntry);

    entries[path] = std::move(newEntry);

    stbi_image_free(imageData);
    return {
        .imageView = imageView,
        .sampler = sampler
    };
}


// move everything that needs uploading from staging to device local mem
void ResourceCache::uploadPending() {
    if ( !this->stagingDirty )
        return;

    VK_ASSERT( vkEndCommandBuffer(this->stagingCommandBuffer) );

    VkSubmitInfo submitInfo {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &this->stagingCommandBuffer,
    };

    VK_ASSERT( vkQueueSubmit(application->graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE) );

    std::cout << "submit staging commands" << std::endl;

    this->stagingDirty = false;
}


// should clean up staging command process:
// uploadPending: end recording, queue submit, mark clean
//      dont start recording immediately
//      first transfer will start recording, mark dirty
//      if cmdBuf is pending (don't know how to check), block (or use more in pool)



} // namespace VkRenderer
