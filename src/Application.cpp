#include <glm/ext/matrix_float4x4.hpp>
#define INIT_GLOBALS
#include "Common.h"

#include "Application.hpp"
#include "ResourceCache.hpp"
#include "Utility.hpp"
#include "VulkanDestroyer.h"
#include "config.hpp"
#include "vk_mem_alloc.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"

#include <memory>
#include <vector>
#include <filesystem>
#include <GLFW/glfw3.h>
#include <cstring>
#include <dlfcn.h>
#include <functional>
#include <string>
#include <thread>
#include <iostream>
#include <unordered_map>
#include <optional>
#include <cassert>
#include <vulkan/vulkan_core.h>


namespace VkRenderer {

Application::Application() {}

Application::~Application() {
    vkDeviceWaitIdle(*this->device);
}


void Application::init() {
    initGLFW();
    loadGlobalFunctions();
    createInstance();
    loadInstanceFunctions();
    createWindow();
    createSurface();
    choosePhysicalDeviceAndQueueFamilies();
    createLogicalDevice();
    loadDeviceFunctions();
    initVmaAllocator();
    initGlobalContext();
    registerGLfwCallbacks();
    getQueues();
    swapchainManager.init(*this->device, this->physicalDevice, this->window, *this->surface);
    assert(swapchainManager.createSwapchain());
    initResourceCache();
    initDSTemplates();
    createGraphicsPipelineLayout();
    createRenderpass();
    createGraphicsPipeline();
}

void Application::initGLFW() {
    glfwInit();
    assert( glfwVulkanSupported() );

    uint32_t count;
    const char** extensions = glfwGetRequiredInstanceExtensions(&count);
    
    for (uint32_t i = 0; i < count; i++)
        desiredInstanceExtensions.push_back(extensions[i]);
}


void Application::loadGlobalFunctions() {
    *this->library = dlopen( "libvulkan.so.1", RTLD_NOW );
    assert(*this->library != nullptr);

    // load loader function from vulkan dynamic library
    #define EXPORTED_VULKAN_FUNCTION( name ) \
        name = (PFN_##name) dlsym( *this->library, #name ); \
        assert( name != nullptr );

    // load global level functions
    #define GLOBAL_LEVEL_VULKAN_FUNCTION( name ) \
        name = (PFN_##name) vkGetInstanceProcAddr( nullptr, #name ); \
        assert( name != nullptr);

    #include "ListOfVulkanFunctions.inl"
}


void Application::createInstance() {
    //VkPhysicalDeviceFeatures desiredDeviceFeatures = {};

    VkApplicationInfo applicationInfo = {
        VK_STRUCTURE_TYPE_APPLICATION_INFO,
        nullptr,
        "vk renderer",
        VK_MAKE_VERSION(1, 0, 0),
        "engine1",
        VK_MAKE_VERSION(1, 0, 0),
        API_VERSION
    };

    VkInstanceCreateInfo instanceCreateInfo = {
        VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        nullptr,
        0,
        &applicationInfo,
        (uint32_t) desiredInstanceLayers.size(),
        desiredInstanceLayers.data(),
        (uint32_t) desiredInstanceExtensions.size(),
        desiredInstanceExtensions.size() > 0 ? &desiredInstanceExtensions[0] : nullptr
    };

    VK_ASSERT( vkCreateInstance(&instanceCreateInfo, nullptr, this->instance.ptr()));
}


void Application::loadInstanceFunctions() {
    #define INSTANCE_LEVEL_VULKAN_FUNCTION( name ) \
    name = (PFN_##name) vkGetInstanceProcAddr( *this->instance, #name ); \
    assert(name != nullptr);
    
    #include "ListOfVulkanFunctions.inl"

    #define INSTANCE_LEVEL_VULKAN_FUNCTION_FROM_EXTENSION( name, extension ) \
    for (auto & enabled : desiredInstanceExtensions ) { \
        if ( !strcmp(enabled, extension ) ) { \
        name = (PFN_##name) vkGetInstanceProcAddr( *this->instance, #name); \
        assert(name != nullptr); \
        } \
    }
    
    #include "ListOfVulkanFunctions.inl"
}


void Application::createWindow() {
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    this->window = glfwCreateWindow(initial_window_size.width, initial_window_size.height, "vk-renderer", NULL, NULL);
    assert( this->window );
}

void Application::createSurface() {
    VK_ASSERT( glfwCreateWindowSurface(*instance, window, NULL, &(*this->surface)) );
}


static void framebufferResizeCallback(GLFWwindow *window, int width, int height) {
    //std::cout << "Application: resize callback" << std::endl;
    Application *app = reinterpret_cast<Application *>(glfwGetWindowUserPointer(window));
    app->swapchainManager.windowResized = true;
}

void Application::registerGLfwCallbacks() {
    glfwSetWindowUserPointer(this->window, this);
    glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);
}


static std::vector<VkQueueFamilyProperties> getQueueFamilies(VkPhysicalDevice device) {
    uint32_t numFamilies;
    std::vector<VkQueueFamilyProperties> families;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &numFamilies, nullptr);
    families.resize(numFamilies);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &numFamilies, families.data());

    return families;
}


static bool selectFamily(VkPhysicalDevice device, VkFlags flags, uint32_t & resultFamily) {
    std::vector<VkQueueFamilyProperties> families = getQueueFamilies(device);

    for (uint32_t i = 0; i < families.size(); i++) {
        VkQueueFamilyProperties & family = families[i];
        if ( family.queueCount > 0 && 
            (family.queueFlags & flags) == flags ) {
            resultFamily = i;
            return true;
        }
    }
    return false;
}


static bool selectPresentFamily(VkInstance instance, VkPhysicalDevice device, VkSurfaceKHR surface, uint32_t & resultFamily) {
    std::vector<VkQueueFamilyProperties> families = getQueueFamilies(device);

    for (uint32_t i = 0; i < families.size(); i++) {
        if ( glfwGetPhysicalDevicePresentationSupport(instance, device, i) ) {
            VkBool32 supported;
            VkResult res = vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &supported);
            if (res == VK_SUCCESS && supported) {
                resultFamily = i;
                return true;
            }
        }
    }
    return false;
}


void Application::choosePhysicalDeviceAndQueueFamilies() {
    std::vector<VkPhysicalDevice> devices;
    uint32_t numDevices;
    vkEnumeratePhysicalDevices(*this->instance, &numDevices, nullptr);
    devices.resize(numDevices);
    vkEnumeratePhysicalDevices(*this->instance, &numDevices, devices.data());

    this->physicalDevice = VK_NULL_HANDLE;
    for (VkPhysicalDevice currentDev: devices) {
        if ( !selectFamily(currentDev, VK_QUEUE_GRAPHICS_BIT, this->graphicsFamily) )
            continue;
        if ( !selectFamily(currentDev, VK_QUEUE_COMPUTE_BIT, this->computeFamily) )
            continue;
        if ( !selectPresentFamily(*this->instance, currentDev, *this->surface, this->presentFamily))
            continue;

        this->physicalDevice = currentDev;
        break;
    }
    assert( this->physicalDevice != VK_NULL_HANDLE );
}


void Application::createLogicalDevice() {
    float prio[1];
    prio[0] = 1.0f;
    std::vector<VkDeviceQueueCreateInfo> queueInfos = {
        {
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .queueFamilyIndex = graphicsFamily,
            .queueCount = 1,
            .pQueuePriorities = prio,
        }
    };

    if (graphicsFamily != presentFamily) {
    queueInfos.push_back({
        .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .queueFamilyIndex = presentFamily,
        .queueCount = 1,
        .pQueuePriorities = prio,
        });
    }

    VkPhysicalDeviceVulkan12Features features_12 {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES,
        .descriptorIndexing     = VK_TRUE,
        .runtimeDescriptorArray = VK_TRUE,

    };

    VkPhysicalDeviceFeatures deviceFeatures = { .depthBounds = VK_TRUE };
    VkDeviceCreateInfo deviceCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pNext = &features_12,
        .flags = 0,
        .queueCreateInfoCount = (uint32_t) queueInfos.size(),
        .pQueueCreateInfos = queueInfos.data(),
        .enabledLayerCount = 0,
        .ppEnabledLayerNames = nullptr,
        .enabledExtensionCount = (uint32_t) desiredDeviceExtensions.size(),
        .ppEnabledExtensionNames = desiredDeviceExtensions.data(),
        .pEnabledFeatures = &deviceFeatures,
    };
    VK_ASSERT( vkCreateDevice(this->physicalDevice, &deviceCreateInfo, nullptr, this->device.ptr()) );
};


void Application::loadDeviceFunctions() {
    #define DEVICE_LEVEL_VULKAN_FUNCTION( name ) \
        name = (PFN_##name) vkGetDeviceProcAddr(*this->device, #name); \
        assert(name != nullptr);

    #define DEVICE_LEVEL_VULKAN_FUNCTION_FROM_EXTENSION( name, extension ) \
        for (auto & enabled: desiredDeviceExtensions) { \
            if ( !strcmp(enabled, extension) ) { \
            name = (PFN_##name) vkGetDeviceProcAddr(*this->device, #name); \
            assert(name != nullptr); \
            } \
        }

    #include "ListOfVulkanFunctions.inl"
}


void Application::initVmaAllocator() {
    VmaVulkanFunctions fp;

    fp.vkGetPhysicalDeviceProperties = vkGetPhysicalDeviceProperties;
    fp.vkGetPhysicalDeviceMemoryProperties = vkGetPhysicalDeviceMemoryProperties;
    fp.vkAllocateMemory = vkAllocateMemory;
    fp.vkFreeMemory = vkFreeMemory;
    fp.vkMapMemory = vkMapMemory;
    fp.vkUnmapMemory = vkUnmapMemory;
    fp.vkFlushMappedMemoryRanges = vkFlushMappedMemoryRanges;
    fp.vkInvalidateMappedMemoryRanges = vkInvalidateMappedMemoryRanges;
    fp.vkBindBufferMemory = vkBindBufferMemory;
    fp.vkBindImageMemory = vkBindImageMemory;
    fp.vkGetBufferMemoryRequirements = vkGetBufferMemoryRequirements;
    fp.vkGetImageMemoryRequirements = vkGetImageMemoryRequirements;
    fp.vkCreateBuffer = vkCreateBuffer;
    fp.vkDestroyBuffer = vkDestroyBuffer;
    fp.vkCreateImage = vkCreateImage;
    fp.vkDestroyImage = vkDestroyImage;
    fp.vkCmdCopyBuffer = vkCmdCopyBuffer;

    fp.vkBindBufferMemory2KHR = vkBindBufferMemory2KHR;
    fp.vkBindImageMemory2KHR  = vkBindImageMemory2KHR;
    fp.vkGetImageMemoryRequirements2KHR = vkGetImageMemoryRequirements2KHR;
    fp.vkGetBufferMemoryRequirements2KHR = vkGetBufferMemoryRequirements2KHR;
    fp.vkGetPhysicalDeviceMemoryProperties2KHR = vkGetPhysicalDeviceMemoryProperties2KHR;

    VmaAllocatorCreateInfo allocatorInfo = {
        .flags = 0,
        .physicalDevice = this->physicalDevice,
        .device = *this->device,
        .pVulkanFunctions = &fp,
        .instance = *this->instance,
        .vulkanApiVersion = API_VERSION,
    };
    VK_ASSERT( vmaCreateAllocator(&allocatorInfo, this->allocator.ptr()) );
}


void Application::initGlobalContext() {
    vmaContext.allocator = *this->allocator;
    vmaContext.device = *this->device;
}


void Application::getQueues() {
    vkGetDeviceQueue(*device, graphicsFamily, 0, &this->graphicsQueue);
    vkGetDeviceQueue(*device, computeFamily, 0, &this->computeQueue);
    vkGetDeviceQueue(*device, presentFamily, 0, &this->presentQueue);
}


static void allocateFramePrimaryCommandBuffer(VkDevice device, uint32_t queueFamily, VkCommandPool & resultPool, VkCommandBuffer & resultBuf) {
    VkCommandPoolCreateInfo poolInfo {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        .queueFamilyIndex = queueFamily
    };
    VK_ASSERT( vkCreateCommandPool(device, &poolInfo, nullptr, &resultPool) );

    VkCommandBufferAllocateInfo bufferInfo {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .commandPool = resultPool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1
    };

    VK_ASSERT( vkAllocateCommandBuffers(device, &bufferInfo, &resultBuf) );
}


void Application::initResourceCache() {
    VkCommandPool pool;
    VkCommandBuffer commandBuffer;
    createCommandPoolBufferPair(pool, commandBuffer);
    cache = std::make_unique<ResourceCache>(this, ~0, pool, commandBuffer);
}


void Application::initDSTemplates() {
    static const uint32_t MAX_SETS = 10;

    // per model, vertex transformation matrix
    std::vector<VkDescriptorPoolSize> mvp_binding_types = {
        {
            VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            1 * MAX_SETS                 // this is confusing
        }
    };
    VkDescriptorPoolCreateInfo mvp_pool_info {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT,
        .maxSets = MAX_SETS, // TODO
        .poolSizeCount = (uint32_t) mvp_binding_types.size(),
        .pPoolSizes = mvp_binding_types.data(),
    };

    VK_ASSERT( vkCreateDescriptorPool(*this->device, &mvp_pool_info, nullptr, this->mvpDS.pool.ptr()) );

    VkDescriptorSetLayoutBinding mvp_binding {
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .pImmutableSamplers = nullptr
    };

    VkDescriptorSetLayoutCreateInfo mvp_layout_info {  // transformation matrix: one change per model
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = 1,
        .pBindings = &mvp_binding
    };

    VK_ASSERT( vkCreateDescriptorSetLayout(*this->device, &mvp_layout_info, nullptr, this->mvpDS.layout.ptr()) );


    // texture lookup array
    std::vector<VkDescriptorPoolSize> tex_lookup_binding_types = {
        {
            VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            256 * MAX_SETS
        }
    };
    VkDescriptorPoolCreateInfo tex_lookup_pool_info {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT,
        .maxSets = MAX_SETS, // TODO
        .poolSizeCount = (uint32_t) tex_lookup_binding_types.size(),
        .pPoolSizes = tex_lookup_binding_types.data(),
    };

    VK_ASSERT( vkCreateDescriptorPool(*this->device, &tex_lookup_pool_info, nullptr, this->texLookupDS.pool.ptr()) );

    VkDescriptorSetLayoutBinding tex_array_binding {
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,  // TODO
        .descriptorCount = 256,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
    };

    VkDescriptorSetLayoutCreateInfo tex_lookup_layout_info {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = 1,
        .pBindings = &tex_array_binding,
    };

    VK_ASSERT( vkCreateDescriptorSetLayout(*this->device, &tex_lookup_layout_info, nullptr, this->texLookupDS.layout.ptr()) );
}


void Application::createRenderpass() {

    VkAttachmentDescription colorAttach {
        .format = this->swapchainManager.format.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
    };

    // for access types (load ops) VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT in VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT
    // don't know if need explicit memory barrier
    //  VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT in VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT

    VkAttachmentDescription depthAttach {
        .format = VK_FORMAT_D32_SFLOAT,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,  // check this
        .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
    };

    std::vector<VkAttachmentDescription> attachments = { colorAttach, depthAttach };

    std::vector<VkAttachmentReference> initialPassColorRefs = {
        {
            0,
            VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        },
    };

    VkAttachmentReference depthRef = {
        1,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    VkSubpassDescription initialPass {
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .colorAttachmentCount = (uint32_t) initialPassColorRefs.size(),
        .pColorAttachments = initialPassColorRefs.data(),
        .pDepthStencilAttachment = &depthRef // TODO
    };
    
    std::vector<VkSubpassDescription> subpasses = { initialPass };

    VkRenderPassCreateInfo passInfo {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .attachmentCount = (uint32_t) attachments.size(),
        .pAttachments = attachments.data(),
        .subpassCount = (uint32_t) subpasses.size(),
        .pSubpasses = subpasses.data(),
        .dependencyCount = 0,
        .pDependencies = nullptr
    };

    VK_ASSERT( vkCreateRenderPass(*this->device, &passInfo, nullptr, this->renderpass.ptr()) );
}


void Application::createGraphicsPipelineLayout() {
    std::vector<VkDescriptorSetLayout> dsLayouts {
        *this->mvpDS.layout,
        *this->texLookupDS.layout
    };

    VkPushConstantRange texIdPushConstant {
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .offset = 0,
        .size = sizeof(int32_t)
    };

    VkPipelineLayoutCreateInfo layoutInfo {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = (uint32_t) dsLayouts.size(),
        .pSetLayouts = dsLayouts.data(),
        .pushConstantRangeCount = 1,
        .pPushConstantRanges = &texIdPushConstant,
    };

    VK_ASSERT( vkCreatePipelineLayout(*this->device, &layoutInfo, nullptr, &(*this->graphicsPipelineLayout)) );
}


void Application::createGraphicsPipeline() {
    VkDestroyer(VkShaderModule) vertexModule;
    *vertexModule = loadShaderModule(*this->device, "spirv/basic.vert.spv");

    VkDestroyer(VkShaderModule) fragModule;
    *fragModule = loadShaderModule(*this->device, "spirv/basic.frag.spv");

    VkPipelineShaderStageCreateInfo vertexStage = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = VK_SHADER_STAGE_VERTEX_BIT,
        .module = *vertexModule,
        .pName = "main",
    };

    VkPipelineShaderStageCreateInfo fragStage = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
        .module = *fragModule,
        .pName = "main",
    };

    std::vector<VkPipelineShaderStageCreateInfo> stages = { vertexStage, fragStage };

    ///////////// vertex input
    VkVertexInputBindingDescription vertexDescription = {
        .binding = 0,  // corresponds to buffer # in vkCmdBindBuffers
        .stride = sizeof(Vertex),
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
    };

    VkVertexInputAttributeDescription positionAttr = {
        .location = 0, // in shader ( location = 0 )
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = 0
    };
    VkVertexInputAttributeDescription texCoordAttr {
        .location = 1,
        .binding = 0,
        .format = VK_FORMAT_R32G32_SFLOAT,
        .offset = 3 * sizeof(float)
    };

    std::vector<VkVertexInputAttributeDescription> attrs = { positionAttr, texCoordAttr };

    VkPipelineVertexInputStateCreateInfo vertexInputInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = 1,
        .pVertexBindingDescriptions = &vertexDescription,
        .vertexAttributeDescriptionCount = (uint32_t) attrs.size(),
        .pVertexAttributeDescriptions = attrs.data(),
    };

    ////////////// input assembly
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = VK_FALSE
    };
    
    ////////////// viewport + scissors
    VkPipelineViewportStateCreateInfo viewportInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .pViewports = nullptr, // dynamic
        .scissorCount = 1,
        .pScissors = nullptr   // dynamic
    };

    //////////// rasterization
    VkPipelineRasterizationStateCreateInfo rasterizationInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_NONE,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .depthBiasConstantFactor = 0.0f,
        .depthBiasClamp = 0.0f,
        .depthBiasSlopeFactor = 0.0f,
        .lineWidth = 1.0f
    };

    ///////////// multisample
    VkPipelineMultisampleStateCreateInfo multisampleInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
        .sampleShadingEnable = VK_FALSE,
        .minSampleShading = 0.0f,
        .pSampleMask = nullptr,
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = VK_FALSE
    };

    ///////////// color blend
    VkPipelineColorBlendAttachmentState attachmentState = {
        .blendEnable = VK_FALSE,
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT
    };

    VkPipelineColorBlendStateCreateInfo blendInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = VK_FALSE,
        .attachmentCount = 1,
        .pAttachments = &attachmentState
    };
    
    std::vector<VkDynamicState> dynamicStates = {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR
    };
    VkPipelineDynamicStateCreateInfo dynamicStateInfo {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .dynamicStateCount = (uint32_t) dynamicStates.size(),
        .pDynamicStates = dynamicStates.data(),
    };

    // TODO add depth test state
    VkPipelineDepthStencilStateCreateInfo depthStateInfo {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS,  // TODO CHECK
        //.depthBoundsTestEnable = VK_TRUE,
        .stencilTestEnable = VK_FALSE,
        .minDepthBounds = 0.0f,
        .maxDepthBounds = 1.0f       
    };

    VkGraphicsPipelineCreateInfo pipelineInfo = {
        VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        nullptr,
        0,
        (uint32_t) stages.size(),
        stages.data(),
        &vertexInputInfo,
        &inputAssemblyInfo,
        nullptr,
        &viewportInfo,
        &rasterizationInfo,
        &multisampleInfo,
        &depthStateInfo,
        &blendInfo,
        &dynamicStateInfo,
        *this->graphicsPipelineLayout,
        *this->renderpass,
        0,
        VK_NULL_HANDLE,
        -1
    };

    VK_ASSERT( vkCreateGraphicsPipelines(*this->device, VK_NULL_HANDLE, 1, &pipelineInfo, 
            nullptr, this->graphicsPipeline.ptr()) );
}


void Application::createFramebuffer(uint32_t swapchainIdx, VkImageView depthView, VkFramebuffer & resultFramebuffer) {
    VkImageView colorView = swapchainManager.images[swapchainIdx].view;

    std::vector<VkImageView> attachments = { colorView, depthView };

    VkFramebufferCreateInfo fbInfo {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .renderPass = *this->renderpass,
        .attachmentCount = (uint32_t) attachments.size(),
        .pAttachments = attachments.data(),
        .width = swapchainManager.extent.width,  // TODO might have to ensure view sizes are still equal to this
        .height = swapchainManager.extent.height,
        .layers = 1,
    };

    VK_ASSERT( vkCreateFramebuffer(*this->device, &fbInfo, nullptr, &resultFramebuffer) );
}


static std::vector<VkDescriptorSet> allocate_descriptor_sets(VkDevice device, DescriptorSetTemplate & tmp, uint n) {
    std::vector<VkDescriptorSetLayout> layout_rpt(n, *tmp.layout);

    VkDescriptorSetAllocateInfo alloc_info {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .descriptorPool = *tmp.pool,
        .descriptorSetCount = n,
        .pSetLayouts = layout_rpt.data(),
    };

    std::vector<VkDescriptorSet> ret(n);

    VK_ASSERT( vkAllocateDescriptorSets(device, &alloc_info, ret.data()) );

    return ret;
}

void Application::allocatePerFrameDescriptorSets() {
    std::vector<VkDescriptorSet> mvp_sets =        allocate_descriptor_sets(*device, mvpDS, frameResources.size());
    std::vector<VkDescriptorSet> tex_lookup_sets = allocate_descriptor_sets(*device, texLookupDS, frameResources.size());

    for (uint32_t i = 0; i < frameResources.size(); i++) {
        frameResources[i].descriptorSets.mvpSet        = mvp_sets[i];
        frameResources[i].descriptorSets.texLookupDSet = tex_lookup_sets[i];
    }
}


void Application::allocateMvpUniformBuffers() {
    VkBufferCreateInfo mvp_buffer_info {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = sizeof(glm::mat4),
        .usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices = &this->graphicsFamily
    };
    
    VmaAllocationCreateInfo mvp_alloc_info {
        .usage = VMA_MEMORY_USAGE_CPU_TO_GPU,
        .requiredFlags = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
    };

    for (uint i = 0; i < frameResources.size(); i++) {
        UniformBuffer buf;
        VK_ASSERT( vmaCreateBuffer(*this->allocator, &mvp_buffer_info, &mvp_alloc_info, &buf.buffer, &buf.allocation, nullptr) );
        VK_ASSERT( vmaMapMemory(*this->allocator, buf.allocation, &buf.mappedMemory) );
        frameResources[i].uniformBuffers.mvpBuffer = std::move(buf);
    }
}

void Application::createDepthBuffers() {
    VkImageCreateInfo depth_info {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = VK_FORMAT_D32_SFLOAT,
        .extent = { swapchainManager.extent.width, swapchainManager.extent.height, 1 },
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT, // TODO
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices = &this->graphicsFamily,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
    };

    VmaAllocationCreateInfo alloc_info {
        .usage = VMA_MEMORY_USAGE_GPU_ONLY,
        .requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
    };

    VkImageViewCreateInfo view_info {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = VK_FORMAT_D32_SFLOAT,
        .components = {
            .r = VK_COMPONENT_SWIZZLE_IDENTITY,
            .g = VK_COMPONENT_SWIZZLE_IDENTITY,
            .b = VK_COMPONENT_SWIZZLE_IDENTITY,
            .a = VK_COMPONENT_SWIZZLE_IDENTITY
        },
        .subresourceRange = {
            .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
            .baseMipLevel = 0,
            .levelCount = VK_REMAINING_MIP_LEVELS,
            .baseArrayLayer = 0,
            .layerCount = VK_REMAINING_ARRAY_LAYERS
        }
    };

    for (uint i = 0; i < this->frameResources.size(); i++) {
        DepthBufferResources handles;
        VK_ASSERT( vmaCreateImage(*this->allocator, &depth_info, &alloc_info, &handles.depthBuffer, 
                    &handles.allocation, nullptr) );
        view_info.image = handles.depthBuffer;
        VK_ASSERT( vkCreateImageView(*this->device, &view_info, nullptr, &handles.depthView) );

        this->frameResources[i].depthBuffer = std::move(handles);
    }
}


void Application::initFrameResources() {
    frameResources.resize(std::min((uint32_t) swapchainManager.images.size(), max_concurrent_frames));

    VkFenceCreateInfo fenceInfo = {
        .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .pNext = nullptr,
        .flags = VK_FENCE_CREATE_SIGNALED_BIT
    };
    VkSemaphoreCreateInfo semInfo = {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
    };

    for (FrameResources & currentFrame: frameResources) {
        VK_ASSERT( vkCreateSemaphore(*device, &semInfo, nullptr, &(*currentFrame.imageAcquired)) );
        VK_ASSERT( vkCreateSemaphore(*device, &semInfo, nullptr, &(*currentFrame.presentable)) );
        VK_ASSERT( vkCreateFence(*device, &fenceInfo, nullptr, &(*currentFrame.imageReleased)) );
        currentFrame.imageIndex = ~0;
        allocateFramePrimaryCommandBuffer(*this->device, this->graphicsFamily, 
                    *currentFrame.commandPool, currentFrame.commandBuffer);
        currentFrame.command_buffer_out_of_date = true;
    }
    allocatePerFrameDescriptorSets();
    allocateMvpUniformBuffers();
    updateMvpDescriptorSets();
    updateTexLookupArrayDescriptorSets();
    createDepthBuffers();
}


void Application::renderloop() {
    initFrameResources();

    if (record_frames_concurrently)
        renderLoopConcurrentFrames();
    else
        renderLoopSequentialFrames();
}


static void resetFrameResource(VkDevice device, FrameResources & resources) {
    vkResetFences(device, 1, &(*resources.imageReleased));
    InitVkDestroyer(device, resources.framebuffer);
}


void Application::renderLoopSequentialFrames() {
    uint32_t frame_index = 0;

    while ( !glfwWindowShouldClose(this->window) ) {
        glfwPollEvents();
        FrameResources & currentFrame = frameResources[frame_index];
        vkWaitForFences(*this->device, 1, &(*currentFrame.imageReleased), VK_FALSE, fence_timeout);
        resetFrameResource(*this->device, currentFrame);
        assert( swapchainManager.acquireImage(*currentFrame.imageAcquired, currentFrame.imageIndex) );
        createFramebuffer(currentFrame.imageIndex, currentFrame.depthBuffer.depthView, *currentFrame.framebuffer);
        updatePerFrameData(currentFrame);
        recordSingleFrame(currentFrame);
        submitSingleFrame(currentFrame);
        frame_index = (frame_index + 1) % frameResources.size();
    }

    glfwDestroyWindow(this->window);
    glfwTerminate();
}


/* concurrent_frames renderloop

This might lead to really bad looking movement with vsync because
    you'll recieve N images and finish working on them soon into the interval before the next frame can be worked on
Although once the application queues up frameResources.size() frames, the presentation engine should be handing swapchain
    images back to the application at a steady rate of the refresh period
I'll have to think about this more

I'll also have to consider doing this at all even without vsync. It might be better to only have cpu concurrency within a single frame's
work, especially when the motion of objects is determined by the application's timer. I'd have to do guesswork to figure out 
what the uniforms/push constants that determine rotation/motion should be by the time a given frame will be displayed, rather
than taking the values when the command buffer is being recorded

This could be useful for some application, probably not a game for example


keep queue of threads for recording each frame's primary command buffer
while ! shouldclose:     // label outer
    check fence
    if unsignaled:
        if queued threads not empty:
            dequeue thread
            join thread
            submit command buffer  // look into batching submits if more frames are ready
            continue     // dont advance position
        Wait for fence
    else:
        dispatch new thread for current position
        add thread to queue
    advance position

*/


void Application::renderLoopConcurrentFrames() {   
}


std::thread Application::dispatchFrameRecording(FrameResources & resources) {
    // something like this, not really necessary yet but I want to keep this
    // convention in case I add other parameters to recordSingleFrame
    auto action = [this, &resources]() {
        recordSingleFrame(resources);
    };
    return std::thread(action);

}


// write to the descriptor sets used for mvp and tex lookup. Link the appropriate buffers
// should happen after scene load
void Application::updateMvpDescriptorSets() {
    std::vector<VkDescriptorBufferInfo> buffer_infos;

    std::vector<VkWriteDescriptorSet> writes;
    for (uint i = 0; i < this->frameResources.size(); i++) {
        VkDescriptorBufferInfo mvp_info {
            .buffer = frameResources[i].uniformBuffers.mvpBuffer.buffer,
            .offset = 0,
            .range = sizeof(glm::mat4)
        };

        buffer_infos.push_back(mvp_info);

        VkWriteDescriptorSet write {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = frameResources[i].descriptorSets.mvpSet,
            .dstBinding = 0,
            .dstArrayElement = 0,
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .pBufferInfo = &mvp_info,
        };

        writes.push_back(write);
    }

    vkUpdateDescriptorSets(*this->device, (uint32_t) writes.size(), writes.data(), 0, nullptr);
}


void Application::updateTexLookupArrayDescriptorSets() {
    assert(this->scene.models.size() == 1);

    std::vector<VkDescriptorImageInfo> texture_infos;

    for (Model & model: scene.models) {
        for (auto & mat: model.materials) {
            VkDescriptorImageInfo mat_info {
                .sampler = mat.diffuseTex.sampler,
                .imageView = mat.diffuseTex.imageView,
                .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
            };
            texture_infos.push_back(mat_info);
        }
    }

    // validation layers complain if I don't init
    while(texture_infos.size() < 256) {
        texture_infos.push_back(texture_infos.back());
    }

    std::vector<VkWriteDescriptorSet> writes;

    for (uint i = 0; i < frameResources.size(); i++) {
        VkWriteDescriptorSet write {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = frameResources[i].descriptorSets.texLookupDSet,
            .dstBinding = 0,
            .dstArrayElement = 0,
            .descriptorCount = (uint32_t) texture_infos.size(),
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .pImageInfo = texture_infos.data(),
        };

        writes.push_back(write);
    }

    vkUpdateDescriptorSets(*this->device, (uint32_t) writes.size(), writes.data(), 0, nullptr);
}


void Application::updatePerFrameData(FrameResources & frame_resources) {
    // mvp should be in per model data but I'll leave it here for now
    // TODO move

    using clock = std::chrono::steady_clock;
    static std::chrono::time_point<clock> start_time = clock::now();

    auto timeNow = std::chrono::steady_clock::now();
    std::chrono::duration<float> fsec = timeNow - start_time;

    glm::mat4 transformation;

    // TODO change 
    glm::mat4 model_trans = glm::identity<glm::mat4>();
    glm::vec3 rotAxis = glm::normalize(glm::vec3(0.2f, 0.5f, 0.1f));
    model_trans = glm::rotate(model_trans, 0.5f * (float) M_PI * fsec.count(), rotAxis);

    glm::mat4 view = glm::identity<glm::mat4>();
    view = glm::translate(view, glm::vec3(0.0f, 0.0f, -10.0f));

    glm::mat4 projection;
    VkExtent2D window_size = this->swapchainManager.extent;
    float aspect = ((float)window_size.width) / window_size.height;
    projection = glm::perspective(glm::radians(45.0f), aspect, 0.1f, 100.0f); 

    transformation = projection * view * model_trans;

    void *memptr = frame_resources.uniformBuffers.mvpBuffer.mappedMemory;
    memcpy(memptr, &transformation, sizeof(transformation));
    
    static bool x = false;
    if ( !x ) {
        std::cout << glm::to_string(transformation) << std::endl;
        x = true;
    }

    // if memtype != COHERENT then invalidate
}

void Application::updatePerModelData(FrameResources & frame_resources, ModelInstance & model) {

}


void Application::recordSingleFrame(FrameResources & resources) {
    VkCommandBufferBeginInfo recordingBeginInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = 0 //VK_COMMAND_BUFFER_USAGE_,
    };

    std::array<VkClearValue, 2> clearVals;
    clearVals[0].color = {{ 0.0f, 0.0f, 0.0f, 1.0f }};
    clearVals[1].depthStencil = {1.0f, 0};

    VkRenderPassBeginInfo rpBeginInfo = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .pNext = nullptr,
        .renderPass = *this->renderpass,
        .framebuffer = *resources.framebuffer,
        .renderArea = { {0, 0}, this->swapchainManager.extent },
        .clearValueCount = (uint32_t) clearVals.size(),
        .pClearValues = clearVals.data(),
    };
    
    VkViewport viewport {
        .x = 0,
        .y = 0,
        .width =  (float) this->swapchainManager.extent.width,
        .height = (float) this->swapchainManager.extent.height,
        .minDepth = 0.0f,
        .maxDepth = 1.0f
    };
    
    VkRect2D scissors {
        .offset = { 0, 0 },
        .extent = this->swapchainManager.extent
    };

    //////////////// BEGIN RECORDING
    vkBeginCommandBuffer(resources.commandBuffer, &recordingBeginInfo);
    vkCmdBeginRenderPass(resources.commandBuffer, &rpBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

    if ( this->presentQueue != this->graphicsQueue ) {

        VkImageMemoryBarrier imageTransition = {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .srcAccessMask = VK_ACCESS_MEMORY_READ_BIT,
            .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
            .oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, // assume render pass already transitioned layout
            .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            .srcQueueFamilyIndex = this->presentFamily,
            .dstQueueFamilyIndex = this->graphicsFamily,
            .image = this->swapchainManager.images[resources.imageIndex].image,
            .subresourceRange = {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = VK_REMAINING_MIP_LEVELS,
                .baseArrayLayer = 0,
                .layerCount = VK_REMAINING_ARRAY_LAYERS,
            }
        };

        vkCmdPipelineBarrier(resources.commandBuffer, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                    0, 0, nullptr, 0, nullptr, 1, &imageTransition);
    }

    vkCmdSetViewport(resources.commandBuffer, 0, 1, &viewport);
    vkCmdSetScissor(resources.commandBuffer, 0, 1, &scissors);

    vkCmdBindPipeline(resources.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, *this->graphicsPipeline);

    std::vector<VkDescriptorSet> sets = { resources.descriptorSets.mvpSet, resources.descriptorSets.texLookupDSet };

    vkCmdBindDescriptorSets(resources.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, *this->graphicsPipelineLayout,
                    0, (uint32_t) sets.size(), sets.data(), 0, nullptr);

    for (ModelInstance &instance: this->scene.instances) {
        Model &model = this->scene.models[instance.modelIdx];
        
        updatePerModelData(resources, instance);

        VkDeviceSize off = 0;
        vkCmdBindVertexBuffers(resources.commandBuffer, 0, 1, &model.vertexBuffer, &off);

        int32_t prev_mat_idx = -1;
        for (auto & face: model.mesh.faces) {
            assert( face.numVertices == 3 );
            
            if (prev_mat_idx != face.matIdx) {
                prev_mat_idx = face.matIdx;
                
                vkCmdPushConstants(resources.commandBuffer, *this->graphicsPipelineLayout,
                                VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(face.matIdx), &face.matIdx);
            }

            // really dumb for now
            vkCmdDraw(resources.commandBuffer, 3, 1, face.firstIdx, 0);
        }
    }


    if ( this->presentQueue != this->graphicsQueue ) {

        VkImageMemoryBarrier imageTransition = {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
            .dstAccessMask = VK_ACCESS_MEMORY_READ_BIT,
            .oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, // assume render pass already transitioned layout
            .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            .srcQueueFamilyIndex = this->graphicsFamily,
            .dstQueueFamilyIndex = this->presentFamily,
            .image = this->swapchainManager.images[resources.imageIndex].image,
            .subresourceRange = {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = VK_REMAINING_MIP_LEVELS,
                .baseArrayLayer = 0,
                .layerCount = VK_REMAINING_ARRAY_LAYERS,
            }
        };

        vkCmdPipelineBarrier(resources.commandBuffer, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                    0, 0, nullptr, 0, nullptr, 1, &imageTransition);
    }

    vkCmdEndRenderPass(resources.commandBuffer);
    vkEndCommandBuffer(resources.commandBuffer);
    ///////////// END RECORDING
}


void Application::submitSingleFrame(FrameResources & resources) {

    VkPipelineStageFlags waitDst = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT; // TODO

    VkSubmitInfo submit_info {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &(*resources.imageAcquired),
        .pWaitDstStageMask = &waitDst,
        .commandBufferCount = 1,
        .pCommandBuffers = &resources.commandBuffer,
        .signalSemaphoreCount = 1,
        .pSignalSemaphores = &(*resources.presentable)
    };

    VK_ASSERT( vkQueueSubmit(this->graphicsQueue, 1, &submit_info, *resources.imageReleased) );

    this->swapchainManager.presentQueueSubmit(resources, this->presentQueue);
}


bool Application::loadScene(const std::vector<std::string> & modelPaths, std::vector<glm::vec3> positions) {
    std::unordered_map<std::string, uint32_t> pathToIdx;

    Scene nextScene = {};

    for (uint32_t i = 0; i < modelPaths.size(); i++) {
        const std::string & path = modelPaths[i];

        auto it = pathToIdx.find(path);
        if ( it != pathToIdx.end() ) {
            ModelInstance instance = {
                .modelIdx = it->second,
                .scenePosition = positions[i]
            };
            nextScene.instances.push_back(instance);
        } else {
            std::optional<Model> model = loadModel(path, true);
            if ( !model ) {
                std::cout << "Failed to load scene" << std::endl;
                return false;
            }
            uint32_t idx = nextScene.models.size();

            pathToIdx[path] = idx;

            nextScene.models.push_back(std::move(*model));

            ModelInstance instance = {
                .modelIdx = idx,
                .scenePosition = positions[i]
            };
            nextScene.instances.push_back(instance);
        }
    }

    this->scene = nextScene;
    this->cache->uploadPending();

    return true;
}


std::optional<Model> Application::loadModel(const std::string& path, bool triangulate) {
    Model model = {};
    
    tinyobj::attrib_t attribute;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string error;
    
    std::string mtlBasedir = std::filesystem::path(path).parent_path();
    mtlBasedir += "/";

    bool res = tinyobj::LoadObj(&attribute, &shapes, &materials, &error, path.c_str(), 
            mtlBasedir.c_str(), 
            triangulate);

    if ( !res ) {
        std::cout << "Failed to load obj file " << path << ": " << error << std::endl;
        return std::nullopt;
    }

    if ( !error.empty())
        std::cout << "loadModel: " << error << std::endl;

    uint dataIdx = 0;
    for (tinyobj::shape_t const& s: shapes) {
        // per face
        uint shapeMeshIdx = 0;
        uint faceIdx = 0;
        for (uint fv : s.mesh.num_face_vertices) {
            
            Face f = {
                .firstIdx = dataIdx,
                .numVertices = fv,
                .matIdx = s.mesh.material_ids[faceIdx],
            };
            model.mesh.faces.push_back(f);

            // per vertex
            for (uint i = 0; i < fv; i++) {
                tinyobj::index_t idx = s.mesh.indices[shapeMeshIdx];
                Vertex v = {
                    .x = attribute.vertices[3 * idx.vertex_index],
                    .y = attribute.vertices[3 * idx.vertex_index + 1],
                    .z = attribute.vertices[3 * idx.vertex_index + 2],
                    .tx = attribute.texcoords[3 * idx.texcoord_index],
                    .ty = attribute.texcoords[3 * idx.texcoord_index + 1],
                };
                model.mesh.data.push_back(v);
                shapeMeshIdx++;
            }

            std::cout << "mat id: " << s.mesh.material_ids[faceIdx] << std::endl;

            dataIdx += fv;
            faceIdx++;
            
            //TODO
            // material data is per-face
            // shapes[s].mesh.material_ids[f];
        }
    }

    std::optional<VkBuffer> deviceVBO = cache->putVertexBuffer(path, model.mesh.data);
    assert(deviceVBO);

    model.vertexBuffer = *deviceVBO;

    // TODO
    for (const tinyobj::material_t& mat: materials) {
        if ( !mat.diffuse_texname.empty() ) {
            ResourceCache::TextureResult tex_result = this->cache->getTexture(mat.diffuse_texname);
            Material newMat;
            newMat.diffuseTex.imageView = tex_result.imageView;
            newMat.diffuseTex.sampler   = tex_result.sampler;

            model.materials.push_back(newMat);
        }
    }

    std::cout << "model: " <<std::endl;
    std::cout << model.toString() << std::endl;

    return model;
}

}  // namespace VkRenderer